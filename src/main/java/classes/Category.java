package classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Category {
	
	//variables
	public int idCategory;
	public String name;
	
	
	public  ArrayList<Category> GetCategory() {
		try{
					    
		    //Connection
		    Connection conn =  DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");
		   
		    //createStatement 
		    Statement stmt = conn.createStatement();
		     
			//SQL STRING
			String sql  = "SELECT * from categories";
			
			// execute the query, and get a java ResultSet
			ResultSet rs = stmt.executeQuery(sql);
	       
	        //create list product
			ArrayList<Category> categories = new ArrayList<Category>();
	        
	        while(rs.next())
	        {		        
	        	Category item =new Category();

	        	item.idCategory = Integer.parseInt(rs.getString("idCategory"));
	        	item.name = rs.getString("name");	 
	        	categories.add(item);
	        }
	        
	        //close recordset
	        rs.close();
	        
	        return categories;
	    }
	    catch(Exception ex)
	    {
	    	System.out.println(ex.getMessage());
	    	return null;
	    }
	}

}